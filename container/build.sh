#!/bin/bash

set -eux

root_dir=$PWD

apk add --no-cache go git crun busybox ca-certificates e2fsprogs parted gpgme util-linux conmon make bash xz curl jq qemu qemu-system-aarch64 qemu-system-arm qemu-system-x86_64 qemu-system-riscv64 qemu-img python3 py3-click py3-rich py3-pip

wget -O /usr/bin/shunit2 https://raw.githubusercontent.com/kward/shunit2/v2.1.8/shunit2

apk add --no-cache -t build_deps linux-headers util-linux patch gcc libseccomp-dev meson linux-pam-dev libcap-ng-dev bison musl-dev cargo buildah protoc

# UPX cannot be found on non-x86_64 platforms, so allow it to fail
apk add --no-cache -t upx || /bin/true

if [ "${ARCH}" == "amd64" ]; then
    # Build the test containers, which is only needed for the integration tests
    mkdir -p /test-containers/
    echo '[storage]
      driver = "vfs"
      runroot = "/var/lib/containers/storage/runroot"
      graphroot = "/var/lib/containers/storage/graphroot"
    ' > /etc/containers/storage.conf

    echo -e "\n# Building the exit-container"
    gcc -s -o /test-containers/exit-container $root_dir/container/exit-container.c -static -Os
    container_id=$(buildah from --isolation=chroot scratch)
    buildah copy $container_id /test-containers/exit-container /exit-container
    buildah config --cmd '[ "/exit-container" ]' $container_id
    buildah commit $container_id exit-container:latest
    buildah push exit-container:latest oci-archive:/test-containers/exit-container.tar:exit-container:latest

    # Install the valve-infra executor client which will be used by the smoke tests
    pip install valve-gfx-ci.executor.client
fi

# Util-linux
git clone --depth=1 https://github.com/util-linux/util-linux.git /src/util-linux
cd /src/util-linux
meson setup --auto-features=disabled --buildtype=minsize build .
ninja -C build unshare
strip -s build/unshare
cp build/unshare /usr/bin/myunshare

# Build fscryptctl
git clone --depth=1 https://github.com/google/fscryptctl.git /src/fscryptctl
cd /src/fscryptctl
make fscryptctl
strip -s fscryptctl
cp fscryptctl /bin/fscryptctl
cd ..

# Build podman
# TODO: Compile podman from a glibc-based distro, as musl is apparently unsupported:
# https://github.com/containers/podman/issues/12563#issuecomment-992725650
git clone --depth 1 --branch v4.5 https://github.com/containers/podman.git /src/podman/
cd /src/podman
patch -p1 < $root_dir/patches/podman/0001-WIP-Try-resolving-missing-env-files-as-volume-paths.patch
make EXTRA_LDFLAGS="-w -s" BUILDTAGS="btrfs_noversion containers_image_openpgp exclude_graphdriver_aufs exclude_graphdriver_btrfs exclude_graphdriver_devicemapper exclude_graphdriver_zfs seccomp" podman
cp bin/podman /bin/podman
cd ..

# Make cargo use the git command to work around libgit2 resource usage
mkdir -p ~/.cargo/
echo -e "[net]\ngit-fetch-with-cli = true" > ~/.cargo/config

# Build netavark
git clone --depth 1 --branch v1.6.0 https://github.com/containers/netavark.git /src/netavark/
cd /src/netavark/
patch -p1 < $root_dir/patches/netavark/0001-cargo-minimize-the-size-of-the-binary.patch
make
strip -s bin/netavark
upx --best --lzma bin/netavark || /bin/true  # Some platforms may not have UPX support, make this step optional
mkdir -p /usr/lib/podman/
cp bin/netavark /usr/lib/podman/netavark
cd ..

# Remove all the unecessary files
rm -rf /src/* /root/.cache

# Download mcli
release=RELEASE.2023-05-18T16-59-00Z
wget -O - https://github.com/minio/mc/archive/refs/tags/${release}.tar.gz | tar xz
mv /src/mc-${release} /src/mc/
patch -d /src/mc/ -p1 < $root_dir/patches/mc/0001-mirror-allow-changing-the-working-directory-before-r.patch

# Build u-root
cd /src
git clone https://github.com/u-root/u-root.git /src/u-root
cd /src/u-root
go build

apk del build_deps

# UPX may not have been installed, so allow its deletion to fail
apk del upx || /bin/true
